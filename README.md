# Sitefinity NuGet Setup

*DRAFT*

Describes setup steps required to start a clean Sitefinity site using only 
NuGet package manager for binaries.

## Steps

0. Configure [Sitefinity NuGet Server](https://docs.sitefinity.com/sitefinity-cms-nuget-packages-repository) 
as a package source.
0. In Visual Studio, select File > New Project.
0. Select Web > ASP.NET Web Application (.NET Framework).
    * Name: `SitefinityWebApp`
    * Location: <any>
    * Solution Name: Your.Solution.Name
    * Framework: 4.5+ (.NET 4.7.1 for Sitefinity 11.0+)
0. Select an Empty ASP.NET template
    * Folders and References: MVC
    * Authentication: None
0. Upgrade all default NuGet packages.
0. In the project root, create a folder called `Mvc`.
Move `Controllers`, `Models`, and `Views` into `Mvc`.
0. Delete unused files:
    * Global.asax(.cs)
    * App_Start/RouteConfig.cs
0. Open NuGet Package Manager and install`Telerik.Sitefinity.All`.
    * Note: Package version *must* match license version!
0. **TODO:** In project root, create `default.aspx`.
0. **TODO:** Copy critical web.config stanzas.
0. **TODO:** Add resource setting to `Properties/AssemblyInfo.cs`
0. *Optional:* Use LFS for binary files:
    0. Open command line to solution root.
    0. Type `git lfs install`.
    0. Type `git lfs track *.<extension>` (e.g., `git lfs track *.zip`)
0. Update gitignore:
    * Sitefinity.lic
    * Logs/
    * Storage/